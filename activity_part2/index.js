// Part 2: Simple Server - Node.js

/*
	1. Create an index.js file inside of the activity folder. 
	2.Import the http module using the required directive. Create a variable port and assign it with the value of 8000.
	3. Create a server using the createServer method that will listen in to the port provided above.
	4. Console log in the terminal a message when the server is successfully running.
	5. Create a condition that when the login route, the register route, the homepage and the usersProfile route is accessed, it will print a message specific to the page accessed.
	6. Access each routes to test if it’s working as intended. Save a screenshot for each test.
	7. Create a condition for any other routes that will return an error message.

*/
// Code here:


let http = require('http')

var port = 8000

const server = http.createServer(function(req, res){
	if (req.url == '/login'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Welcome to the login page!')

	} else if (req.url == '/register'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Register Here')

	} else if (req.url == '/homepage'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('HOMEPAGE')

	} else if (req.url == '/userProfile'){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Your Profile')

	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end('Page not available')
	}

})

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`)